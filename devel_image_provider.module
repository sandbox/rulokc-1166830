<?php
/**
 * @file
 * To allow choose image provider in content generation.
 */
 
/**
 * Implementation of hook_menu
 */
function devel_image_provider_menu() {
  $items['admin/generate/image_provider'] = array(
    'title' => 'Image Providers',
    'description' => t('Allow choose image provider in content generation'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('devel_image_provider_providers_form'),
    'access arguments' => array('administer nodes'),
  );
  return $items;
}

function devel_image_provider_providers_form() {
  $form['provider'] = array(
    '#type' => 'radios',
    '#title' => t('Choose image provider to use in content generation'),
    '#default_value' => variable_get(
      'fieldfield_image_generator_function', 
      '_filefield_generate_image'),
  );
  $form['provider']['#options'] = module_invoke_all('devel_image_provider');
  
  $form['get_method'] = array(
    '#type' => 'select',
    '#title' => t('Method to get files'),
    '#description' => t('In case a external provider is selected'),
    '#default_value' => variable_get(
      'devel_image_provider_get_method', 
      curl),
    '#options' => array(
      'curl' => 'cURL',
      'gd' => 'gd',
      'file_get_contents' => 'file_get_contents',
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );
  
  return $form;
}

/**
 * Implementation of hook_devel_image_provider
 * For case default
 */
function devel_image_provider_devel_image_provider() {
  $item = array(
    '_filefield_generate_image' => 'Filefield (' . t('default') . ')'
  );
  return $item;
}

function devel_image_provider_providers_form_submit($form_id, &$form_state) {
  switch ($form_state['values']['op']) {
    case t('Save'):
      variable_set('fieldfield_image_generator_function', $form_state['values']['provider']);
      variable_set('devel_image_provider_get_method', $form_state['values']['get_method']);
      drupal_set_message(t('Settings saved.'));
      break;
    case t('Reset'):
      variable_set('fieldfield_image_generator_function', '_filefield_generate_image');
      variable_set('devel_image_provider_get_method', curl);
      drupal_set_message(t('Settings restored to defaults.'));
      break;
  }
}

/**
 * Implementation of hook_form_alter.
 *//*
function devel_image_provider_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'devel_generate_content_form':
      $form['devel_image_provider'] = array(
        '#type' => 'item',
        '#title' => 'Function to generate image',
        '#value' => variable_get('fieldfield_image_generator_function', '_filefield_generate_image'),
        '#description' => t('Set this in !url', array('!url' => l(t('Devel Image Provider settings'), 'admin/generate/image_provider'))),
      );
      // to show submit button at end
      $form['submit']['#weight'] = 1;
      break;
  }
}*/

/**
 * Basic function to generate image.
 * Adapt it to specific cases.
 */
function devel_image_provider_generate_image($extension, $min_resolution, $max_resolution, $provider_base_url) {
  $min = explode('x', $min_resolution);
  $max = explode('x', $max_resolution);

  $width = rand((int)$min[0], (int)$max[0]);
  $height = rand((int)$min[0], (int)$max[0]);
      
  if ($temp_file = tempnam(file_directory_temp(), 'lpx')) {
    file_move($temp_file, $temp_file . '.' . $extension);
    
    $url = "$provider_base_url/$width/$height";
    
    return devel_image_provider_get_file($url, $temp_file);
  }
}

/**
 * Return file of $url using selected get_method
 */
function devel_image_provider_get_file($url, $filename) {
  $method = variable_get('devel_image_provider_get_method', 'curl');
  
  $data = NULL;
  
  switch ($method) {
    case 'curl':
      $data = devel_image_provider_file_get_data_curl($url);
      file_put_contents($filename, $data);
      break;
    case 'gd':
      $image = imagecreatefromjpeg($url);
      imagejpeg($image, $filename);
      imagedestroy($image);
      break;
    case 'file_get_contents':
      $data = file_get_contents($url);
      file_put_contents($filename, $data);
      break;
  }
  
  return $filename;
}

/**
 * Gets the data from a URL
 * http://davidwalsh.name/download-urls-content-php-curl
 * TODO: FIX PROBLEM sometimes not work for lorempixum provider
 * (get 301 Moved Permanently error message).
 */
function devel_image_provider_file_get_data_curl($url) {
  $ch = curl_init();
  $timeout = 5;
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
  $data = curl_exec($ch);
  curl_close($ch);
  return $data;
}