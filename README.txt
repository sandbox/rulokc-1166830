DEVEL IMAGE PROVIDER
--------------------

Allow choose image provider in content generation.


Installation
------------

- Is required this patch for FileField:
  
  http://drupal.org/files/issues/fieldfield_image_generator_function-1134986-4511674.patch
  
  More information here: http://drupal.org/node/1134986#comment-4512820
 
  function _filefield_generate_file($field) {
    ...
    elseif (in_array($extension, array('png', 'jpg')) && function_exists('imagecreate')) {
      $min_resolution = empty($field['widget']['min_resolution']) ? '100x100' : $field['widget']['min_resolution'];
      $max_resolution = empty($field['widget']['max_resolution']) ? '600x600' : $field['widget']['max_resolution'];
      //return _filefield_generate_image($extension, $min_resolution, $max_resolution);
      $fieldfield_image_generator_function = variable_get('fieldfield_image_generator_function', '_filefield_generate_image');
      return $fieldfield_image_generator_function($extension, $min_resolution, $max_resolution);
    }
    
    return FALSE;
  }

- Install module as usual.

Use
---

- Inside admin/generate, look Image Providers to choose image provider
  to use in content generation.
  
  Developers can add new providers through specific module.
  
  Is provided some modules, as example of support for:
  
  lorempixum.com
  flickholdr.com
  placekitten.com
  
  TODO: Make more clean presentation of specific settings of providers
  
- Inside admin/generate/content, generate items as usual.

Issues
------

- For lorempixum, sometimes getting image with curl not work.

Thanks
------
To ipwa, marvil07 and Drupal Perú community for the idea and support.

Author
------
Antonio Kobashikawa Carrasco (Rulo)
akobashikawa@gmail.com